package br.com.trackmap.br.com.trackmap.Models;

public class PedidosEntrega {

    private int numPedidosEntrega;
    private int numTotalPedidos;

    public int getNumPedidosEntrega() {
        return numPedidosEntrega;
    }

    public void setNumPedidosEntrega(int numPedidosEntrega) {
        this.numPedidosEntrega = numPedidosEntrega;
    }

    public int getNumTotalPedidos() {
        return numTotalPedidos;
    }

    public void setNumTotalPedidos(int numTotalPedidos) {
        this.numTotalPedidos = numTotalPedidos;
    }


}
