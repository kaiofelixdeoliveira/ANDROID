package br.com.trackmap.br.com.trackmap.Models;

public class Entrega {

    private int protocoloEntrega;
    private String endereco;
    private String cidade;
    private int numero;
    private String estado;
    private String pontoRef;
    private String horario;
    private String status;
    private String prazoEntrega;

    public int getProtocoloEntrega() {
        return protocoloEntrega;
    }

    public void setProtocoloEntrega(int protocoloEntrega) {
        this.protocoloEntrega = protocoloEntrega;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPontoRef() {
        return pontoRef;
    }

    public void setPontoRef(String pontoRef) {
        this.pontoRef = pontoRef;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrazoEntrega() {
        return prazoEntrega;
    }

    public void setPrazoEntrega(String prazoEntrega) {
        this.prazoEntrega = prazoEntrega;
    }
}
