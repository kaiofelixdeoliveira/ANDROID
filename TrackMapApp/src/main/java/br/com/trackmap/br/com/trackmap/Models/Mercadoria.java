package br.com.trackmap.br.com.trackmap.Models;


public class Mercadoria {


    private String peso;
    private String volume;
    private String periculosidade ;
    private  String numNtFiscal;
    private String codMerc;
    private String tipo ;


    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getPericulosidade() {
        return periculosidade;
    }

    public void setPericulosidade(String periculosidade) {
        this.periculosidade = periculosidade;
    }

    public String getNumNtFiscal() {
        return numNtFiscal;
    }

    public void setNumNtFiscal(String numNtFiscal) {
        this.numNtFiscal = numNtFiscal;
    }

    public String getCodMerc() {
        return codMerc;
    }

    public void setCodMerc(String codMerc) {
        this.codMerc = codMerc;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }



}
