package br.com.trackmap.br.com.trackmap.App;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import br.com.trackmap.R;

public class ScreenMain extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
