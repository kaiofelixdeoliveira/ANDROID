package br.com.trackmap.br.com.trackmap.Models;

public class Veiculo {

    private String numPlaca;
    private String cor;
    private String modelo;
    private String pesoTot;
    private String tipo;

    public String getNumPlaca() {
        return numPlaca;
    }

    public void setNumPlaca(String numPlaca) {
        this.numPlaca = numPlaca;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPesoTot() {
        return pesoTot;
    }

    public void setPesoTot(String pesoTot) {
        this.pesoTot = pesoTot;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }



}
