package br.com.trackmap.br.com.trackmap.Models;

public class CadastroFisico extends Cadastro {
    private String cpf;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}
