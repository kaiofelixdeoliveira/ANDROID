package br.com.trackmap.br.com.trackmap.Models;

public class CadastroJuridico extends Cadastro {

    private String cnpj;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
}
